http://treelog.azurewebsites.net/

I think it's time to acknowledge that I will not be continuing this project a.k.a. I have achieved what I set out to do.

The goal of this project was to understand and implement an authentication system via Owin. The authentication works just fine. Also, I wanted to user bootstrap / jquery to quickly stand up the site and provide a usable look and feel. Finally, I wanted to use the Google web components technology to create a simple template for displaying photos and info of logged trees.

I also decided to learn about blob servers on azure as well as a few other azure features like continuous deployment.

I had hoped to eventually use the API in a mobile app, perhaps written using Xamarin for both iOS and Android - but ultimately I will be tackling that technology in a different application.

Known issues: 

* When you register the application will send you an email - but the link in the email does not redirect back to the site (it goes to a blank page). 
* There are problems with editing photos (client side implementation is not complete).
* The drop down list for tree species is incomplete (only a partial test list was used).
* The drop down list for parks in Columbus is incomplete.
* The look and feel of the website is not done (recently started using LESS, should have used from the beginning).

The purpose of this application was to provide a friendly way to record information about Ohio tree species that people find in Columbus parks. The goal was to eventually provide mobile support so that species of trees can be looked up, described and their location cataloged. 

The TODO list is still quite long - but some features that would make the app useful would be:

* leaf / bark pictures for each species of tree
* location awareness of trees between users to prevent duplication
* forums or message capability for users to discuss findings

This project occupied an hour of my morning for a couple of months and was pretty fun to work on. 
              