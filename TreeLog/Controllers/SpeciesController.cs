﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TreeLogData;

namespace TreeLog.Controllers
{
    [AllowAnonymous]
    [Route("api")]
    public class SpeciesController : ApiController
    {
        [Route("species")]
        public IHttpActionResult GetSpecies()
        {
            using (var context = new TreeLogEntities())
            {
                var speciesEntities = from species in context.Species
                                  select species;

                var speciesList = Models.Species.ReturnSpeciesModelList(speciesEntities);
                if (speciesList.Count > 0)
                {
                    return Ok(speciesList);
                }
            }

            return NotFound();
        }

        [Route("speciesById")]
        public IHttpActionResult GetSpeciesById(int id)
        {
            using (var context = new TreeLogEntities())
            {
                var speciesEntity = (from species in context.Species
                                    where species.Id == id
                                    select species).First();

                if (speciesEntity != null)
                {
                    var speciesModel = Models.Species.ReturnSpeciesModel(speciesEntity);
                    return Ok(speciesModel);
                }
            }

            return NotFound();
        }
    }
}
