﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using TreeLogData;
using TreeLog.Services;


namespace TreeLog.Controllers
{
    [Authorize]
    [Route("api")]
    public class TreePictureController : ApiController
    {
        [Route("{treeId}/picture")]
        public IHttpActionResult GetTreePicture(int treeId)
        {
            using (var context = new TreeLogEntities())
            {
                var picturePath = (from path in context.TreePictures
                               where path.TreeId == treeId
                               select path).First();
                if (picturePath != null)
                {
                    return Ok(picturePath);
                }
                else
                {
                    return NotFound();
                }
            }
        }

        [Route("picture")]
        public async Task<HttpResponseMessage> PostPicture()
        {
            var savedFilePath = new List<Uri>();
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            try
            {                
                var content = await Request.Content.ReadAsMultipartAsync();

                var username = (from name in content.Contents
                                where name.Headers.ContentDisposition.Name == "\"username\""
                               select name).First().ReadAsStringAsync().Result;

                var path = await PictureStorageService.UploadPhoto(content, username);
                savedFilePath.Add(path);

                return Request.CreateResponse(HttpStatusCode.Created, savedFilePath);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }
    }
}
