﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TreeLogData;

namespace TreeLog.Controllers
{
    [AllowAnonymous]
    [Route("api")]
    public class LocationController : ApiController
    {
        [Route("locations")]
        public IHttpActionResult GetLocations()
        {
            using (var context = new TreeLogEntities())
            {
                var locationEntities = from locationEntity in context.Locations
                                       select locationEntity;

                var locationList = Models.Location.ReturnLocationModelList(locationEntities);

                if (locationList.Count > 0)
                {
                    return Ok(locationList);
                }
            }

            return NotFound();
        }

        [Route("location")]
        public IHttpActionResult GetLocation(int id)
        {
            using (var context = new TreeLogEntities())
            {
                var locationEntity = (from entity in context.Locations
                                      where entity.Id == id
                                      select entity).First();
                if (locationEntity != null)
                {
                    var location = Models.Location.ReturnLocationModel(locationEntity);
                    return Ok(location);
                }
            }

            return NotFound();
        }
    }
}
