﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TreeLog.Models;
using TreeLogData;

namespace TreeLog.Controllers
{
    [AllowAnonymous]
    [Route("api")]
    public class HealthController : ApiController
    {
        [Route("healths")]
        public IHttpActionResult GetHealths()
        {            
            using (var context = new TreeLogEntities())
            {
                var healthQuery = from healths in context.Healths
                                 select healths;

                var healthList = Models.Health.ReturnHealthModelList(healthQuery);

                if (healthList.Count > 0)
                {
                    return Ok(healthList);
                }
                
            }

            return NotFound();
        }

        [Route("health")]
        public IHttpActionResult GetHealth(int id)
        {
            using (var context = new TreeLogEntities())
            {
                var healthEntity = (from entity in context.Healths
                                    where entity.Id == id
                                    select entity).First();

                if (healthEntity != null)
                {
                    var health = Models.Health.ReturnHealthModel(healthEntity);
                    return Ok(health);
                }                                
            }

            return NotFound();
        }
    }
}
