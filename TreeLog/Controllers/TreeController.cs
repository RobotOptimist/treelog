﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TreeLog.Services;
using TreeLogData;
using System.Diagnostics;

namespace TreeLog.Controllers
{
    [Authorize]
    [Route("api")]
    public class TreeController : ApiController
    {
        [Route("trees")]
        public IHttpActionResult GetTrees()
        {
            using (var context = new TreeLogEntities())
            {
                var treeList = (from trees in context.Trees
                                select trees).ToList();
                if (treeList.Count > 0)
                {
                    return Ok(treeList);
                }
            }

            return NotFound();
        }

        [Route("{username}/trees")]
        public IHttpActionResult GetTreesByUser(string username)
        {
            using (var context = new TreeLogEntities())
            {
                var treeListByUser = (from trees in context.GetTreesByUser(username)
                                      select trees).ToList();                                    

                if (treeListByUser.Count > 0)
                {
                    return Ok(Models.TreeInput.CreateTreeInputList(treeListByUser));
                }
            }

            return NotFound();
        }

        [Route("trees/{treeId}")]
        public IHttpActionResult GetTreeById(int treeId)
        {
            using (var context = new TreeLogEntities())
            {
                var tree = (from trees in context.Trees
                            where trees.Id == treeId
                            select trees).First();

                if (tree != null)
                {
                    return Ok(tree);
                }
            }

            return NotFound();
        }

        [Route("createTree")]
        public IHttpActionResult CreateTree(Models.TreeInput tree)
        {
            using (var context = new TreeLogEntities())
            {
                try
                {
                    tree.Id = decimal.ToInt32(context.CreateTree(tree.Diameter, tree.HealthId, tree.LocationId, tree.SpeciesId, tree.UserId).First().Value);
                    var username = (from user in context.AspNetUsers
                                    where user.Id == tree.UserId
                                    select user).First().UserName;

                    var treeId = tree.Id ?? default(int);

                    var uri = PictureStorageService.UpLoadPhoto(username, tree.PhotoPath, treeId);

                    context.StorePictureLocation(treeId, uri.ToString());
                                        
                    return Ok(tree);
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                    return InternalServerError();
                    throw;
                }
                
            }

        }

        [Route("editTree")]
        public IHttpActionResult EditTree(Tree editTree)
        {
            using (var context = new TreeLogEntities())
            {
                var changeTrees = from trees in context.Trees
                                  where trees.Id == editTree.Id
                                  select trees;

                foreach (var tree in changeTrees)
                {
                    if (!tree.Equals(editTree))
                    {
                        tree.Diameter = editTree.Diameter;
                        tree.Health = editTree.Health;
                        tree.Health_Id = editTree.Health_Id;
                        tree.Location = editTree.Location;
                        tree.Location_Id = editTree.Location_Id;
                        tree.Species = editTree.Species;
                        tree.Species_Id = editTree.Species_Id;
                    }
                }

                try
                {
                    context.SaveChanges();
                    return Ok();
                }
                catch (Exception)
                {
                    return InternalServerError();
                    throw;
                }

            }
        }

        [Route("deleteTree")]
        public IHttpActionResult DeleteTree(Tree tree)
        {
            using (var context = new TreeLogEntities())
            {
                context.Trees.Remove(tree);

                try
                {
                    context.SaveChanges();
                    return Ok();
                }
                catch (Exception)
                {                    
                    return InternalServerError();
                    throw;
                }
            }
        }
    }
}
