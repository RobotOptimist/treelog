﻿/// <reference path="jquery-2.2.0.js" />

'use strict';
const apiObjects = (function () {
    let tree = {
        id: null,
        userId: null,
        diameter: null,
        speciesId: null,
        healthId: null,
        locationId: null,
        photoPath: null
    };

    let species = {
        id: null,
        name: null
    };

    let location = {
        id: null,
        name: null,
        zipcode: null
    };
    let health = {
        id: null,
        status: null
    };

    const populateTree = function (diameter, speciesId, healthId, locationId, userId, photo) {        
        tree.userId = userId;
        tree.diameter = diameter;
        tree.speciesId = speciesId;
        tree.healthId = healthId;
        tree.locationId = locationId;
        tree.photoPath = photo
        return tree;
    };

    function populateSpecies(id, name) {
        species.id = id;
        species.name = name;
        return species;
    };

    function populateLocation(id, name, zipcode) {
        location.id = id;
        location.name = name;
        location.zipcode = zipcode;
        return location;
    };

    function populateHealth(id, status) {
        health.id = id;
        health.status = status;
        return health;
    };

    return {
        tree: populateTree,
        species: populateSpecies(),
        location: populateLocation(),
        health: populateHealth()
    };
})();

const apiService = (function () {
    const url = (function () {

        const getTreesByUser = function (username) {
            return username + '/trees';
        };

        const getPictureByTree = function (treeId) {
            return String(treeId) + '/picture';
        };

        const getTrees = 'trees';        
        const getSpecies = 'species';
        const getHealths = 'healths';
        const getLocations = 'locations';
        const postTree = 'createTree';
        const postPicture = 'picture';
        return {
            allTrees: getTrees,
            treesByUser: getTreesByUser,
            species: getSpecies,
            healths: getHealths,
            locations: getLocations,
            postTree: postTree,
            pictureByTree: getPictureByTree,
            postPicture: postPicture
        }
    })();

    let token = "";
    let headers = {};

    const setToken = function () {
        token = sessionStorage.getItem("tokenKey");
        headers = {
            Authorization: 'Bearer ' + token
        };
    }

    console.log(headers);

    const $gettingSpecies = function () {
        return $.ajax({
            type: 'GET',
            url: url.species,
            headers: headers,
            contentType: 'application/json; charset=utf-8'
        });

    };

    const $gettingLocations = function () {
        return $.ajax({
            type: 'GET',
            url: url.locations,
            headers: headers,
            contentType: 'application/json; charset=utf-8'
        });
    };

    const $gettingHealths = function () {
        return $.ajax({
            type: 'GET',
            url: url.healths,
            headers: headers,
            contentType: 'application/json; charset=utf-8'
        });
    };

    const $gettingTrees = function () {
        return $.ajax({
            type: 'GET',
            url: url.allTrees,
            headers: headers,
            contentType: 'application/json; charset=utf-8'
        });
    };

    const $gettingTreesByUser = function (username) {
        return $.ajax({
            type: 'GET',
            url: url.treesByUser(username),
            headers: headers,
            contentType: 'application/json; charset=utf-8'
        });
    };

    const $gettingTreePictureByTree = function (treeId) {
        return $.ajax({
            type: 'GET',
            url: url.pictureByTree(treeId),
            headers: headers,
            contentType: 'application/json; charset=utf-8'
        });
    };

    const $postingTree = function (tree) {
        return $.ajax({
            type: 'POST',
            url: url.postTree,
            headers: headers,            
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(tree)
        });
    };

    const $postingPicture = function (formData) {
        return $.ajax({
            url: url.postPicture,
            headers: headers,
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false
        });
    };

    return {
        $gettingTrees: $gettingTrees,
        $gettingTreesByUser: $gettingTreesByUser,
        $gettingTreePictureByTree: $gettingTreePictureByTree,
        $gettingHealths: $gettingHealths,
        $gettingSpecies: $gettingSpecies,
        $gettingLocations: $gettingLocations,
        $postingTree: $postingTree,
        $postingPicture : $postingPicture,
        setToken:setToken
    };
})();

const cachedApiData = (function () {
    const healthData = localStorage.getItem("healths");
    const speciesData = localStorage.getItem("species");
    const locationData = localStorage.getItem("locations");
    return {
        healths: healthData,
        locations: locationData,
        species: speciesData
    };
})();