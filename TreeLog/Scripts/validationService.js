﻿'use strict';
const validate = (function () {

    //server side validation also in effect for all of these
    function validateEmail(email) {
        let re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (re.test(email)) {
            return email;
        }
        else {
            console.log("Invalid email");
        }
    }

    function validatePassword(password) {
        let splitPassword = password.split('');
        if (splitPassword.length < 6) {
            return console.error("password is too small");
        }
        return password;
    }

    function validatePassword2(password, password2) {
        if (password == password2) {
            return password2;
        }
        return console.error("passwords do not match");        
    }

    return {
        email: validateEmail,
        password: validatePassword,
        password2: validatePassword2
    }

})();