﻿/// <reference path="jquery-2.2.0.min.js" />
/// <reference path="validationService.js" />

'use strict';
const authenticationObjects = (function () {

    let registerData = {
        Email: null,
        Username: null,
        Firstname: null,
        Lastname: null,
        Password: null,
        ConfirmPassword: null
    };

    let loginData = {
        grant_type: 'password',
        username: null,
        password: null        
    }

    const populateRegisterData = function (email, userName, firstName, lastName, password, password2) {
        registerData.Email = validate.email(email);
        registerData.Username = userName;
        registerData.Firstname = firstName;
        registerData.Lastname = lastName;
        registerData.Password = validate.password(password);
        registerData.ConfirmPassword = validate.password2(password, password2);

        for (var data in registerData) {
            if (!registerData.hasOwnProperty(data)){
                continue;
            }

            if (data === null || data === ''){
                return console.error("Data Required");
            }
        }

        return registerData;
    }

    const populateLoginData = function (username, password) {
        loginData.username = username;
        loginData.password = validate.password(password);        

        for (var data in loginData) {
            if (!loginData.hasOwnProperty(data)) {
                continue;
            }

            if (data === null || data === '') {
                return console.error("Data Required");
            }
        }

        return loginData;
    }

    return {
        registerData: populateRegisterData,
        loginData: populateLoginData
    }

})()

const serverAuthentication = (function () {

    const url = (function () {
        const register = '/api/Account/Register';
        const login = '/oauth/token';

        return {
            register: register,
            login: login
        }
    })();

    const $registering = function (registerData) {
        return $.ajax({
            type: 'POST',
            url: url.register,
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(registerData)
        });
    };

    const $loggingIn = function (loginData) {
        return $.ajax({
            type: 'POST',
            url: url.login,            
            data: loginData
        })
    };

    return {
        $registering: $registering,
        $loggingIn: $loggingIn
    }

})();