﻿/// <reference path="jquery-2.2.0.min.js" />
/// <reference path="apiService.js" />
'use strict';
const treeList = function () {

    apiService.$gettingTreesByUser(sessionStorage.getItem("username")).done(function (data) {        
        for (var i = 0; i < data.length; i++) {
            let clone = cloneTree.clone(data[i].locationId, data[i].healthId, data[i].speciesId, data[i].diameter, data[i].photoPath, data[i].id);
            let container = document.getElementById('treeList');
            container.insertBefore(clone, container.firstChild);
        }
    });
};

const cloneTree = (function () {
    const treeTemplate = document.getElementById("treeTemplate").import;
    const template = treeTemplate.getElementById("postedTree");

    const resolveDataFromId = function (data, id, propertyName) {
        data = JSON.parse(data);
        for (var i = 0; i < data.length; i++) {
            if (data[i].id == id) {
                return data[i][propertyName];
            }
        }
    };

    const handleTreeData = function (location, health, species, diameter, photo, id) {
        let clone = document.importNode(template.content, true);
        let deleteId = "delete" + id;
        let editId = "edit" + id;
        clone.querySelector(".delete").setAttribute("id", deleteId);
        clone.querySelector(".edit").setAttribute("id", editId);
        clone.querySelector(".templatePhoto").src = photo;
        clone.querySelector(".health").innerText = resolveDataFromId(cachedApiData.locations, location, "name");
        clone.querySelector(".location").innerText = resolveDataFromId(cachedApiData.healths, health, "status");
        clone.querySelector(".species").innerText = resolveDataFromId(cachedApiData.species, species, "name");
        clone.querySelector(".diameter").innerText = diameter;
        return clone;
    }

    return {
        clone: handleTreeData
    }

})();