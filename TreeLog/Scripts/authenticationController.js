﻿/// <reference path="jquery-2.1.4.min.js" />
/// <reference path="authenticationService.js" />
'use strict';

$(function () {
    $("#registerModal").on("click", "#registerBtn", function () {
        console.log("register clicked!");        
        let registerData = authenticationObjects.registerData($("#registerEmail").val(), $("#registerUsername").val(), $("#registerFirstName").val(), $("#registerLastName").val(), $("#registerPassword").val(), $("#registerPasswordConfirm").val());
        serverAuthentication.$registering(registerData).done(function () {
            console.log("registered successfully");
            $("#register-modal").modal("hide");
        }).fail(function () { console.error("problem registering"); });
    });

    $("#loginModal").on("click", "#loginBtn", function () {
        console.log("login clicked!");
        let loginData = authenticationObjects.loginData($("#username").val(), $("#password").val());
        serverAuthentication.$loggingIn(loginData).done(function (data) {
            sessionStorage.setItem("tokenKey", data.access_token);
            sessionStorage.setItem("username", data.username);
            sessionStorage.setItem("userId", data.userId);
            console.log("logged in successfully");
            let welcomeMessage = "Welcome " + sessionStorage.getItem("username");
            $("#welcome").html(welcomeMessage);
            $("#login-modal").modal("hide");
            $("#login").html("Log out");
            $("#login").attr("data-target", "");
            $("#login").attr("id", "logout");
            loadTreeInterface();
        }).fail(function () { console.error("problem logging in");});
    });

    $("#login-modal").on("hidde.bs.modal", function () {
        let token = sessionStorage.getItem("tokenKey");
        if (token != null) {
            $("#login").html("Log out");
            $("#login").attr("id", "logout");
        }
    });

    $(document).on("click", "#logout", function () {
        sessionStorage.setItem("tokenKey", null);
        sessionStorage.setItem("username", null);
        sessionStorage.setItem("userId", null);
        $("#welcome").html("");
        $("#add").html("<p>Please log in to add trees.</p>");
        $("#list").html("<p>Please log in to list trees</p>")
        $("#logout").html("Log in");
        $("#logout").attr("id", "login");
        $("#login").attr("data-target", "#login-modal");
    });
});