﻿/// <reference path="apiService.js" />

'use strict';
$(document).ready(function () {
    $("#home").load("../Views/Home/Home.html", function () {
        console.log("Home loaded.");
    });

    $("#about").load("../Views/About/About.html", function () {
        console.log("About loaded.");
    });

    $("#add").html("<p>Please log in to add trees.</p>");
    $("#list").html("<p>Please log in to list trees</p>")
      
    $("#loginModal").load("../Views/Modals/_Login.html", function () {
        console.log("_Login modal loaded");
    });

    $("#registerModal").load("../Views/Modals/_Register.html", function () {
        console.log("_Register modal loaded");
    });

    $("#uploadModal").load("../Views/Modals/_Upload.html", function () {
        console.log("_Upload modal loaded");
    });


    let tokenKey = sessionStorage.getItem("tokenKey");

    if (typeof(tokenKey) == 'string' && tokenKey === 'null') {
        tokenKey = null;
    };

    if (tokenKey) {
        $("#login").html("Log out");
        $("#login").attr("data-target", "");
        $("#login").attr("id", "logout");
        let welcomeMessage = "Welcome " + sessionStorage.getItem("username");
        $("#welcome").html(welcomeMessage);
        loadTreeInterface();
    } else {
        $("#welcome").html("");
    };

});

const loadTreeInterface = function () {
    apiService.setToken();
    $("#add").load("../Views/AddTree/AddTree.html", function () {
        console.log("AddTree loaded.");

        addTree();

        $("#list").load("../Views/TreeList/TreeList.html", function () {
            console.log("TreeList loaded");
            treeList();
        });
    });
};