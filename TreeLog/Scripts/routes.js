﻿function clearPanel() {

}

Path.map("#/home").to(function () {
    $("#home").removeClass("hidden");
    $("#about").removeClass("hidden");
    $("#add").addClass("hidden");
    $("#list").addClass("hidden");
});

Path.map("#/addtree").to(function () {
    $("#add").removeClass("hidden");
    $("#list").removeClass("hidden");
    $("#home").addClass("hidden");
    $("#about").addClass("hidden");
});

Path.map("#/trees").to(function () {
    $("#list").removeClass("hidden");
    $("#home").addClass("hidden");
    $("#add").addClass("hidden");
    $("#about").addClass("hidden");
});

Path.map("#/about").to(function () {
    $("#home").removeClass("hidden");
    $("#about").removeClass("hidden");
    $("#add").addClass("hidden");
    $("#list").addClass("hidden");
});

Path.root("#/home");

Path.listen();