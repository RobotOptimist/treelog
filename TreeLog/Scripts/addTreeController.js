﻿/// <reference path="jquery-2.2.0.min.js" />
/// <reference path="apiService.js" />
/// <reference path="treeListController.js" />

'use strict';
const addTree = function () {    
    const selectLists = (function () {
        const healthSelect = "#healthSelect";
        const locationSelect = "#locationSelect";
        const speciesSelect = "#speciesSelect";
        return {
            health: healthSelect,
            location: locationSelect,
            species: speciesSelect
        };
    })();

    function optionCreator(id, name) {
        let selector = '<option value="' + id + '">' + name + '</option>';
        return selector;
    };

    function addListOfOptions(selectList, optionList) {
        for (var i = 0; i < optionList.length; i++) {         
            $(selectList).append(optionList[i]);
        };
    };

    function handleApiDataForSelect(selector, data, propertyName) {
        data = JSON.parse(data);        
        let list = [];
        for (var i = 0; i < data.length; i++) {
            list.push(optionCreator(data[i].id, data[i][propertyName]));
        }
        addListOfOptions(selector, list);
    }

    function clearTreeData() {
        $(selectLists.species).val(1);
        $(selectLists.health).val(1);
        $(selectLists.location).val(1);
        $("#diameterInput").text("");
        $("#photo").attr("src", "");
        $("#photoIdent").show();
    }

    if (cachedApiData.healths) {
        handleApiDataForSelect(selectLists.health, cachedApiData.healths, "status");
    }
    else {
        apiService.$gettingHealths().done(function (data) {
            data = JSON.stringify(data);
            handleApiDataForSelect(selectLists.health, data, "status");
            localStorage.setItem("healths", data);
        });
    };
    
    if (cachedApiData.species) {
        handleApiDataForSelect(selectLists.species, cachedApiData.species, "name");
    }
    else {
        apiService.$gettingSpecies().done(function (data) {
            data = JSON.stringify(data);
            handleApiDataForSelect(selectLists.species, data, "name");
            localStorage.setItem("species", data);
        });
    };
    
    if (cachedApiData.locations) {
        handleApiDataForSelect(selectLists.location, cachedApiData.locations, "name");
    }
    else {
        apiService.$gettingLocations().done(function (data) {
            data = JSON.stringify(data);
            handleApiDataForSelect(selectLists.location, data, "name");
            localStorage.setItem("locations", data);
        });
    };    

    $("#add").on("click", "#saveTree", function () {
        const userId = sessionStorage.getItem("userId");
        let diameter = $("#diameterInput").val();
        let species = $("#speciesSelect").val();
        let health = $("#healthSelect").val();
        let location = $("#locationSelect").val();
        let photo = sessionStorage.getItem("photo");
        let tree = apiObjects.tree(diameter, species, health, location, userId, photo);

        apiService.$postingTree(tree).done(function (data) {                        
            let clone = cloneTree.clone(data.locationId, data.healthId, data.speciesId, data.diameter, data.photoPath);
            let container = document.getElementById('treeList');
            container.insertBefore(clone, container.firstChild);
            clearTreeData();
        });
    });

    $("#uploadModal").on("click", "#uploadButton", function () {
        if ($("#file-upload").val() == "") {
            console.log("No file selected");
            return;
        };

        let formData = new FormData();
        let file = $("#file-upload")[0];

        let username = sessionStorage.getItem("username");

        formData.append("username", username);
        formData.append("file", file.files[0]);

        apiService.$postingPicture(formData).done(function (data) {            
            
            sessionStorage.setItem("photo", data[0]);
            $("#photoIdent").hide();
            $("#photo").attr("src", data[0]);            
        });

        $("#upload-modal").modal('hide');
    });
};