namespace TreeLog.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<TreeLog.Models.ApplicationUser.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(TreeLog.Models.ApplicationUser.ApplicationDbContext context)
        {
            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationUser.ApplicationDbContext()));

            var user = new ApplicationUser()
            {
                UserName = "jmac",
                Email = "james.macivor1@gmail.com",
                EmailConfirmed = true,
                FirstName = "James",
                LastName = "MacIvor"
            };

            manager.Create(user, "123456");
        }
    }
}
