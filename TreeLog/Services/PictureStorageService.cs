﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace TreeLog.Services
{
    public static class PictureStorageService
    {

        private static CloudBlobContainer createContainer(string username)
        {
            CloudStorageAccount pictureAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["TreePictureConnect"]);

            CloudBlobClient blobClient = pictureAccount.CreateCloudBlobClient();

            CloudBlobContainer container = blobClient.GetContainerReference(username);            

            container.CreateIfNotExists();

            container.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });

            return container;
        }

        public static Uri UpLoadPhoto(string username, string filePath, int treeId)
        {
            var container = createContainer(username);
            
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(treeId.ToString());
            Uri uriFilePath;

            if (Uri.IsWellFormedUriString(filePath, UriKind.Absolute))
            {
                uriFilePath = new Uri(filePath);
            }
            else
            {
                return null;
            }

            try
            {
                using (var response = WebRequest.Create(uriFilePath).GetResponse())
                    using(var stream = response.GetResponseStream())
                    blockBlob.UploadFromStreamAsync(stream);                
                               
                return blockBlob.Uri;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                throw;
            }
            
        }

        public static async Task<Uri> UploadPhoto(MultipartMemoryStreamProvider content, string username)
        {
            var container = createContainer(username);
            var blockBlob = container.GetBlockBlobReference("0");
            
            try
            {               
                using (var stream = await (from file in content.Contents
                                           where file.Headers.ContentDisposition.Name == "\"file\""
                                           select file).First().ReadAsStreamAsync()) 
                     await blockBlob.UploadFromStreamAsync(stream);
                                
                return blockBlob.Uri;
            }
            catch (Exception e)
            {
                throw;
            }                      
        }
        
    }
}