﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TreeLog.Models
{
    public class Tree
    {
        public int Id { get; set; }
        public Location Location { get; set; }
        public Species Species { get; set; }
        public ApplicationUser User { get; set; }
        public float Diameter { get; set; }
        public Health Health { get; set; }
    }

    public class TreeInput
    {
        public int? Id { get; set; }
        public int? LocationId { get; set; }
        public int? SpeciesId { get; set; }
        public int? HealthId { get; set; }
        public float? Diameter { get; set; }
        public string UserId { get; set; }
        public string PhotoPath { get; set; }

        public static TreeInput PopulateTreeFromEntity(TreeLogData.GetTreesByUser_Result treeEntity)
        {
            return new TreeInput
            {
                Id = treeEntity.Id,
                LocationId = treeEntity.Location_Id,
                SpeciesId = treeEntity.Species_Id,
                HealthId = treeEntity.Health_Id,
                Diameter = treeEntity.Diameter,
                UserId = treeEntity.User_Id,
                PhotoPath = treeEntity.FilePath
            };
        }

        public static List<TreeInput> CreateTreeInputList(List<TreeLogData.GetTreesByUser_Result> treeEntityList)
        {
            List<TreeInput> treeInputList = new List<TreeInput>();
            foreach (var treeEntity in treeEntityList)
            {
                treeInputList.Add(PopulateTreeFromEntity(treeEntity));
            }
            return treeInputList;
        }
    }

    public class TreePicture
    {
        public int Id { get; set; }
        public int TreeId { get; set; }
        public string FilePath { get; set; }
    }
}