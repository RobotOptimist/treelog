﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TreeLog.Models
{
    public class Species
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public static Species ReturnSpeciesModel(TreeLogData.Species species)
        {
            return new Species
            {
                Id = species.Id,
                Name = species.Name
            };
        }

        public static List<Species> ReturnSpeciesModelList(IQueryable<TreeLogData.Species> speciesEntities)
        {
            List<Species> speciesList = new List<Species>();

            foreach (var entity in speciesEntities)
            {
                var species = ReturnSpeciesModel(entity);
                speciesList.Add(species);
            }

            return speciesList;
        }
    }
}