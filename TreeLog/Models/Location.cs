﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TreeLog.Models
{
    public class Location
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ZipCode { get; set; }

        public static Location ReturnLocationModel(TreeLogData.Location location)
        {
            return new Location
            {
                Id = location.Id,
                Name = location.Name,
                ZipCode = location.ZipCode
            };
        }

        public static List<Location> ReturnLocationModelList(IQueryable<TreeLogData.Location> locations)
        {
            List<Location> locationList = new List<Location>();

            foreach (var location in locations)
            {
                var locationModel = ReturnLocationModel(location);
                locationList.Add(locationModel);
            }

            return locationList;
        }
    }
}