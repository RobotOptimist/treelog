﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TreeLog.Models
{
    public class Health
    {
        public int Id { get; set; }
        public string Status { get; set; }

        public static Health ReturnHealthModel(TreeLogData.Health entityHealth)
        {
            return new Health
            {
                Id = entityHealth.Id,
                Status = entityHealth.Status
            };
        }

        public static List<Health> ReturnHealthModelList(IQueryable<TreeLogData.Health> entityHealthList)
        {
            List<Health> healthList = new List<Health>();
            foreach (var entityHealth in entityHealthList)
            {
                var health = ReturnHealthModel(entityHealth);
                healthList.Add(health);
            }

            return healthList;
        }
    }
}